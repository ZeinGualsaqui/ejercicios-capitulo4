#Zein Gualsaquí 
#Ejercicio 4 Capitulo 4 texto básico
'''Ejercicio 4: ¿Cuál es la utilidad de la palabra clave “def” en Python?
a) Es una jerga que significa “este código es realmente estupendo”
b) Indica el comienzo de una función
c) Indica que la siguiente sección de código indentado debe ser almacenada para
usarla más tarde
d) b y c son correctas ambas
e) Ninguna de las anteriores'''
print ("La respuesta correcta es el literal d")

#Ejercicio 5 capitulo 4 texto básico 
'''def fred():
    print("Zap")

def jane():
    print("ABC")

jane()
fred()
jane()

a) Zap ABC jane fred jane
b) Zap ABC Zap
c) ABC Zap jane
d) ABC Zap ABC
e) Zap Zap Zap'''
print ("La respuesta correcta es el literal d")
